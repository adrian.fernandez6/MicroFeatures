# MicroFeatures

A POC of using the [uFeatures Architecture](https://docs.tuist.io/building-at-scale/microfeatures) with Tuist

## Getting started

Use `tuist generate --open` to create the project

## uFeatures

This project is modularized using uFeatures. 

A uFeature represents a feature of the App. Each feature is a combination of the following targets:
 - Feature: The source code of the feature
 - Tests: The testing code that verifies that the feature works (Unit and integration)
 - Example: An executable (Xcode Playground) that developers can use to play with the feature (mocking data, overriding the layout direction, colors...)
 - Testing: The mocked data that is used by the Example and Tests frameworks

FeatureExample ---> Feature
FeatureExample ---> FeatureTesting

FeatureTests   ---> Feature 
FeatureTests   ---> FeatureTesting


There are two types of uFeatures:
 - Product features: Those that represent flows of the app
 - Foundation features: Those that include extension or core technologies (Foundation extension, XCTests extensions, UI components...)


## External Dependencies

To integrate external dependencies I've used the Tuist Dependencies.swift manifest.

This manifest lets us integrate SPM and Carthage dependencies to the App.

All the uFeatures use two external dependencies TCA and SwiftUINavigation libraries. Tuist integrates those dependencies as static libraries by default (same as what SPM does) we can make those dynamic if we want. Adding static libraries to all the uFeatures will cause the dependency to be recreated everytime we embed it on a Target. That's why the recommended solution is to create a framework host to add the dependency to that framework, and then import that framework on all the other targets.

## Dependency Graph

[graph](./graph.png)