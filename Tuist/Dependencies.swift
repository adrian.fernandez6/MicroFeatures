import ProjectDescription

let dependencies = Dependencies(
    swiftPackageManager: .init(
        [
            .package(url: "https://github.com/pointfreeco/swift-composable-architecture", .upToNextMajor(from: "0.28.1")),
            .package(url: "https://github.com/pointfreeco/swiftui-navigation", .upToNextMajor(from: "0.1.0")),
        ],
        targetSettings: [
            "ComposableArchitecture": ["IPHONEOS_DEPLOYMENT_TARGET": "15.0"],
            "SwiftUINavigation": ["IPHONEOS_DEPLOYMENT_TARGET": "15.0"],
        ]
    ),
    platforms: [.iOS]
)
