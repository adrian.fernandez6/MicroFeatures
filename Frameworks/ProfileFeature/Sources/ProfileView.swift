import ComposableArchitecture
import ComposableArchitectureFramework
import SwiftUI

enum ProfileSection: CaseIterable {
    case profile
    case messages
    case switchEvent
    case settings

    var name: String {
        switch self {
        case .profile:
            return "Edit Profile"
        case .messages:
            return "Messages"
        case .switchEvent:
            return "Switch Event"
        case .settings:
            return "App Settings"
        }
    }

    var icon: String {
        switch self {
        case .profile:
            return "pencil"
        case .messages:
            return "envelope"
        case .switchEvent:
            return "arrowshape.turn.up.left"
        case .settings:
            return "gear"
        }
    }
}

public struct State: Equatable, Hashable {
    public init() {}
}

public enum Action: Equatable {
    case logout
    case dismiss
}

public let reducer = Reducer<State, Action, Void> { state, action, _ in
    switch action {
    case .logout:
        return .none
    case .dismiss:
        return .none
    }
}

final class BundleType {}

public struct ProfileView: View {
    private let store: Store<State, Action>

    public init(
        store: Store<State, Action>
    ) {
        self.store = store
    }

    public var body: some View {
        NavigationView {
            WithViewStore(store) { viewStore in
                List {
                    Section {
                        HStack {
                            Spacer()
                            VStack {
                                Image("ProfileImage", bundle: Bundle(for: BundleType.self))
                                    .resizable()
                                    .frame(width: 90, height: 90)
                                    .clipShape(Circle())
                                Text("Janne Doe")
                                    .font(.title3)
                                    .fontWeight(.semibold)
                            }
                            Spacer()
                        }
                    }
                    Section {
                        ForEach(ProfileSection.allCases, id: \.name) { section in
                            NavigationLink {
                                if case .settings = section {
                                    List {
                                        HStack {
                                            Image(systemName: "info.circle")
                                            Text("App Info")
                                                .fontWeight(.semibold)
                                        }

                                        HStack {
                                            Spacer()
                                            Button {
                                                withAnimation {
                                                    viewStore.send(.logout)
                                                }
                                            } label: {
                                                Text("Log out")
                                                    .foregroundColor(.red)
                                                    .fontWeight(.semibold)
                                            }
                                            Spacer()
                                        }
                                    }
                                    .listStyle(.insetGrouped)
                                } else {
                                    Text(section.name)
                                }
                            } label: {
                                HStack {
                                    Image(systemName: section.icon)
                                    Text(section.name)
                                        .fontWeight(.semibold)
                                }
                            }
                        }
                    }
                }
                .navigationBarTitle("Account", displayMode: .inline)
                .listStyle(.insetGrouped)
                .toolbar {
                    ToolbarItem(placement: .primaryAction) {
                        Button {
                            withAnimation {
                                viewStore.send(.dismiss)
                            }
                        } label: {
                            Text("Done")
                                .foregroundColor(.primary)
                                .fontWeight(.bold)
                        }
                    }
                }
            }
        }
    }
}
