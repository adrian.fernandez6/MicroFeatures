import PlaygroundSupport
import ProfileFeature
import SwiftUI

PlaygroundPage.current.setLiveView(
    ProfileFeature.ProfileView(
        store: .init(
            initialState: .init(),
            reducer: ProfileFeature.reducer,
            environment: Void()
        )
    )
)
