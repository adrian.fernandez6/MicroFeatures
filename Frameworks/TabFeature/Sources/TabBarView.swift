import ComposableArchitecture
import ComposableArchitectureFramework
import SwiftUI
import ProfileFeature
import SwiftUINavigation
import SwiftUINavigationFramework

public enum Router: Equatable {
    case profile
    case chat
}

public enum Tab: String, CaseIterable {
    case home
    case schedule
    case explore

    var image: String {
        switch self {
        case .home: return "house"
        case .schedule: return "calendar"
        case .explore: return "rectangle.grid.2x2"
        }
    }
}

public struct State: Equatable, Hashable {
    @BindableState var tab: Tab
    @BindableState var router: Router?
    var profile: ProfileFeature.State?

    public init(
        tab: Tab = .home,
        router: Router? = nil,
        profile: ProfileFeature.State? = nil
    ) {
        self._tab = BindableState(wrappedValue: tab)
        self._router = BindableState(wrappedValue: router)
        self.profile = profile
    }
}

public enum Action: BindableAction, Equatable {
    case binding(BindingAction<State>)
    case profile(ProfileFeature.Action)
}

public let reducer = Reducer<State, Action, Void> { state, action, _ in
    switch action {
    case .binding:
        if state.router == .profile {
            state.profile = .init()
        }
        return .none
    case .profile(.logout):
        state.router = nil
        state.profile = nil
        return .none
    case .profile(.dismiss):
        state.router = nil
        state.profile = nil
        return .none
    }
}
.binding()

public struct TabBarView: View {
    private let store: Store<State, Action>

    public init(store: Store<State, Action>) {
        self.store = store
    }

    public var body: some View {
        WithViewStore(store) { viewStore in
            TabView(selection: viewStore.binding(\.$tab)) {
                ForEach(Tab.allCases, id: \.rawValue) { tab in
                    NavigationView {
                        NavigationLink {
                            Text("Second Level")
                        } label: {
                            Text(tab.rawValue.capitalized)
                        }
                        .navigationBarTitle(tab.rawValue.capitalized, displayMode: .large)
                        .toolbar {
                            ToolbarItem(placement: .navigationBarLeading) {
                                Button {
                                    viewStore.send(.binding(.set(\.$router, .profile)))
                                } label: {
                                    Image(systemName: "person.circle")
                                }
                            }

                            ToolbarItem(placement: .navigationBarTrailing) {
                                Button {
                                    viewStore.send(.binding(.set(\.$router, .chat)))
                                } label: {
                                    Image(systemName: "bubble.left")
                                }
                            }
                        }
                    }
                    .navigationViewStyle(.stack)
                    .tabItem {
                        Image(systemName: tab.image)
                    }
                    .tag(tab)
                }
            }
            .sheet(unwrapping: viewStore.binding(\.$router), case: /Router.profile) { _ in
                IfLetStore(store.scope(state: \.profile, action: Action.profile), then: ProfileFeature.ProfileView.init(store:))
            }
            .sheet(unwrapping: viewStore.binding(\.$router), case: /Router.chat) { _ in
                NavigationView {
                    Text("Chat")
                        .toolbar {
                            ToolbarItem(placement: .navigationBarTrailing) {
                                Button {
                                    viewStore.send(.binding(.set(\.$router, nil)))
                                } label: {
                                    Text("Done")
                                        .foregroundColor(.primary)
                                        .fontWeight(.bold)
                                }
                            }
                        }
                }
            }
        }
        
    }

}
