import PlaygroundSupport
import TabFeature
import SwiftUI

PlaygroundPage.current.setLiveView(
    TabFeature.TabBarView(
        store: .init(
            initialState: .init(),
            reducer: TabFeature.reducer,
            environment: Void()
        )
    )
)
