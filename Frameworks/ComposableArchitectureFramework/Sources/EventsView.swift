import SwiftUI

public struct EventsView: View {
    private let tapGesture: () -> Void

    public init(
        tapGesture: @escaping () -> Void
    ) {
        self.tapGesture = tapGesture
    }

    public var body: some View {
        ScrollView {
            LazyVGrid(
                columns: [
                    .init(.adaptive(minimum: 300), spacing: 12)
                ]
            ) {
                ForEach(1...20, id: \.self) { _ in
                    RoundedRectangle(cornerRadius: 32)
                        .fill(Color.purple)
                        .frame(height: 200)
                        .onTapGesture {
                            tapGesture()
                        }
                }
                .padding(12)
            }
        }
        .navigationBarTitle("Welcome to Hopin 👋", displayMode: .large)
    }
}
