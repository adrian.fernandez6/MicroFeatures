import ComposableArchitecture
import ComposableArchitectureFramework
import SwiftUI
import SwiftUINavigation
import SwiftUINavigationFramework
import ProfileFeature

public enum Router: Equatable {
    case profile
}

public struct State: Equatable {
    @BindableState var router: Router?
    var profile: ProfileFeature.State?
    public init(
        router: Router? = nil,
        profile: ProfileFeature.State? = nil
    ) {
        self._router = BindableState(wrappedValue: router)
        self.profile = profile
    }
}

public enum Action: BindableAction, Equatable {
    case binding(BindingAction<State>)
    case onEventTapped
    case profile(ProfileFeature.Action)
}

public let reducer = Reducer<State, Action, Void> { state, action, _ in
    switch action {
    case .binding:
        state.profile = .init()
        return .none
    case .onEventTapped:
        return .none
    case .profile:
        state.router = nil
        return .none
    }
}
.binding()

public struct EventsView: View {
    private let store: Store<State, Action>

    public init(
        store: Store<State, Action>
    ) {
        self.store = store
    }

    public var body: some View {
        WithViewStore(store) { viewStore in
            NavigationView {
                ScrollView {
                    LazyVGrid(
                        columns: [
                            .init(.adaptive(minimum: 300), spacing: 12)
                        ]
                    ) {
                        ForEach(1...20, id: \.self) { _ in
                            RoundedRectangle(cornerRadius: 32)
                                .fill(Color.purple)
                                .frame(height: 200)
                                .onTapGesture {
                                    withAnimation {
                                        viewStore.send(.onEventTapped)
                                    }
                                }
                        }
                        .padding(12)
                    }
                }
                .toolbar {
                    ToolbarItem(placement: .navigationBarLeading) {
                        Button {
                            viewStore.send(.binding(.set(\.$router, .profile)))
                        } label: {
                            Image(systemName: "person.circle")
                        }
                    }
                }
                .navigationBarTitle("Welcome to Hopin 👋", displayMode: .large)
            }
            .sheet(unwrapping: viewStore.binding(\.$router), case: /Router.profile) { _ in
                IfLetStore(store.scope(state: \.profile, action: Action.profile), then: ProfileFeature.ProfileView.init(store:))
            }
        }
    }
}
