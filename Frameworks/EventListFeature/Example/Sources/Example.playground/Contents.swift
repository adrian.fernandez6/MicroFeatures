import PlaygroundSupport
import EventListFeature
import SwiftUI

PlaygroundPage.current.setLiveView(
    EventListFeature.EventsView(
        store: .init(
            initialState: .init(
                router: .none,
                profile: .none
            ), reducer: EventListFeature.reducer,
            environment: Void()
        )
    )
)
