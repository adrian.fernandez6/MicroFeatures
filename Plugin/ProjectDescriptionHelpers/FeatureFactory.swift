import Foundation
import ProjectDescription

public extension Collection where Element == Target {
    static func feature(
        name: String,
        dependencies: [TargetDependency]
    ) -> [Target] {
        [
            Target(
                name: name,
                platform: .iOS,
                product: .framework,
                bundleId: .bundleId(for: name),
                deploymentTarget: .iOS(targetVersion: "15.0", devices: .iphone),
                infoPlist: "Frameworks/\(name)/Info.plist",
                sources: ["Frameworks/\(name)/Sources/**"],
                resources: ["Frameworks/\(name)/Resources/**"],
                dependencies: dependencies
            ),
            Target(
                name: "\(name)Tests",
                platform: .iOS,
                product: .unitTests,
                bundleId: .bundleId(for: "\(name)Tests"),
                infoPlist: "Frameworks/\(name)/Tests.plist",
                sources: "Frameworks/\(name)/Tests/**",
                dependencies: [
                    .target(name: name)
                ]
            ),
            Target(
                name: "\(name)Example",
                platform: .iOS,
                product: .framework,
                bundleId: .bundleId(for: "\(name)Example"),
                deploymentTarget: .iOS(targetVersion: "15.0", devices: .iphone),
                infoPlist: .default,
                sources: ["Frameworks/\(name)/Example/Sources/**"],
                dependencies: [
                    .target(name: name),
                    .target(name: "\(name)Testing")
                ]
            ),
            Target(
                name: "\(name)Testing",
                platform: .iOS,
                product: .framework,
                bundleId: .bundleId(for: "\(name)Testing"),
                deploymentTarget: .iOS(targetVersion: "15.0", devices: .iphone),
                infoPlist: .default,
                sources: ["Frameworks/\(name)/Testing/Sources/**"]
            ),
        ]
    }
}

public extension Target {
    static func frameworkHost(
        dependency name: String
    ) -> Target {
        Target(
            name: "\(name)Framework",
            platform: .iOS,
            product: .framework,
            bundleId: .bundleId(for: "\(name)Framework"),
            deploymentTarget: .iOS(targetVersion: "15.0", devices: .iphone),
            infoPlist: "Frameworks/\(name)Framework/Info.plist",
            sources: ["Frameworks/\(name)Framework/Sources/**"],
            dependencies: [
                .external(name: name)
            ]
        )
    }
}
