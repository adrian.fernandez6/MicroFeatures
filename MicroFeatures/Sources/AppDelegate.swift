import SwiftUI
import EventListFeature
import ComposableArchitectureFramework
import ComposableArchitecture
import TabFeature

enum AppAction: Equatable {
    case events(EventListFeature.Action)
    case tabs(TabFeature.Action)
}

struct AppEnvironment {

}

enum AppState: Equatable {
    case events(EventListFeature.State)
    case tabs(TabFeature.State)
}

let globalReducer = Reducer<AppState, AppAction, AppEnvironment>.combine(
    TabFeature.reducer.pullback(state: /AppState.tabs, action: /AppAction.tabs, environment: { _ in }, breakpointOnNil: false),
    EventListFeature.reducer.pullback(state: /AppState.events, action: /AppAction.events, environment: { _ in }),
    Reducer { state, action, environment in
        switch action {
        case .events(.onEventTapped):
            state = .tabs(.init())
            return .none
        case .events(.profile(.logout)):
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [state] in
                var state = state
                state = .events(.init())
            }
            return .none
        case .events:
            return .none
        case .tabs(.profile(.logout)):
            state = .events(.init())
            return .none
        case .tabs:
            return .none
        }
    }
)

@main
struct NewNavigationApp: App {
    let store = Store<AppState, AppAction>(
        initialState: .events(.init()),
        reducer: globalReducer,
        environment: AppEnvironment()
    )

    var body: some Scene {
        WindowGroup {
            WithViewStore(store) { viewStore in
                SwitchStore(store) {
                    CaseLet(state: /AppState.events, action: AppAction.events, then: EventListFeature.EventsView.init(store:))
                    CaseLet(state: /AppState.tabs, action: AppAction.tabs, then: TabFeature.TabBarView.init(store:))
                }
            }
        }
    }
}
