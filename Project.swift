import ProjectDescription
import BundlePlugin

let eventListFeature: [Target] = .feature(
    name: "EventListFeature",
    dependencies: [
        .target(name: "HopinUI"),
        .target(name: "HopinCore"),
        .target(name: "HopinAPI"),
        .target(name: "ComposableArchitectureFramework"),
        .target(name: "SwiftUINavigationFramework"),
        .target(name: "ProfileFeature")
    ]
)

let exploreFeature: [Target] = .feature(
    name: "ExploreFeature",
    dependencies: [
        .target(name: "HopinUI"),
        .target(name: "HopinCore"),
        .target(name: "HopinAPI"),
        .target(name: "ComposableArchitectureFramework"),
        .target(name: "SwiftUINavigationFramework")
    ]
)

let hopinAPI: [Target] = .feature(
    name: "HopinAPI",
    dependencies: []
)

let hopinCore: [Target] = .feature(
    name: "HopinCore",
    dependencies: []
)

let hopinUI: [Target] = .feature(
    name: "HopinUI",
    dependencies: []
)

let profileFeature: [Target] = .feature(
    name: "ProfileFeature",
    dependencies: [
        .target(name: "HopinUI"),
        .target(name: "HopinCore"),
        .target(name: "HopinAPI"),
        .target(name: "ComposableArchitectureFramework"),
        .target(name: "SwiftUINavigationFramework")
    ]
)

let receptionFeature: [Target] = .feature(
    name: "ReceptionFeature",
    dependencies: [
        .target(name: "HopinUI"),
        .target(name: "HopinCore"),
        .target(name: "HopinAPI"),
        .target(name: "ComposableArchitectureFramework"),
        .target(name: "SwiftUINavigationFramework")
    ]
)

let scheduleFeature: [Target] = .feature(
    name: "ScheduleFeature",
    dependencies: [
        .target(name: "HopinUI"),
        .target(name: "HopinCore"),
        .target(name: "HopinAPI"),
        .target(name: "ComposableArchitectureFramework"),
        .target(name: "SwiftUINavigationFramework")
    ]
)

let tabFeature: [Target] = .feature(
    name: "TabFeature",
    dependencies: [
        .target(name: "HopinUI"),
        .target(name: "HopinCore"),
        .target(name: "HopinAPI"),
        .target(name: "ProfileFeature"),
        .target(name: "ComposableArchitectureFramework"),
        .target(name: "SwiftUINavigationFramework")
    ]
)

let appTargets: [Target] = [
    Target(
        name: "MicroFeatures",
        platform: .iOS,
        product: .app,
        bundleId: .bundleId(for: "MicroFeatures"),
        deploymentTarget: .iOS(targetVersion: "15.0", devices: .iphone),
        infoPlist: .extendingDefault(with: [
            "UIMainStoryboardFile": "",
            "UILaunchStoryboardName": "LaunchScreen"
        ]),
        sources: ["MicroFeatures/Sources/**"],
        resources: ["MicroFeatures/Resources/**"],
        dependencies: [
            .target(name: "ComposableArchitectureFramework"),
            .target(name: "EventListFeature"),
            .target(name: "ExploreFeature"),
            .target(name: "HopinAPI"),
            .target(name: "HopinCore"),
            .target(name: "HopinUI"),
            .target(name: "ProfileFeature"),
            .target(name: "ReceptionFeature"),
            .target(name: "ScheduleFeature"),
            .target(name: "SwiftUINavigationFramework"),
            .target(name: "TabFeature")
        ]
    ),
    Target(
        name: "MicroFeaturesTests",
        platform: .iOS,
        product: .unitTests,
        bundleId: .bundleId(for: "MicroFeaturesTests"),
        infoPlist: "MicroFeatures/Tests.plist",
        sources: "MicroFeatures/Tests/**",
        dependencies: [
            .target(name: "MicroFeatures")
        ]
    ),
    .frameworkHost(dependency: "ComposableArchitecture"),
    .frameworkHost(dependency: "SwiftUINavigation")
]

let targets = appTargets
    + eventListFeature
    + exploreFeature
    + hopinAPI
    + hopinCore
    + hopinUI
    + profileFeature
    + receptionFeature
    + scheduleFeature
    + tabFeature



let project = Project(
    name: "MicroFeatures",
    targets: targets
)
